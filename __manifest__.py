# -*- coding: utf-8 -*-
{
    'name': "iframe_customizer",

    'summary': """
        Gerador de rotas para API manuais e/ou Rotas para Iframes
    """,

    'description': """
        Gerador de rotas para API manuais e/ou Rotas para Iframes
    """,

    'author': "Diogo Berti e Gabriel Balog",
    'category': 'Extra Rights',
    'version': '0.2',

    'depends': ['base','website'],

    'data': [
            'views/custom_views.xml',
            'security/ir.model.access.csv'
    ],
}