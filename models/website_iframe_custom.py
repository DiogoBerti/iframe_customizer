# -*- coding: utf-8 -*-

from odoo import api, fields, models


class WebsiteIframeCustom(models.Model):
    _name = "website.iframe.custom"

    name = fields.Char(string='Nome da Rota')
    url = fields.Char(string='Rota')
    redirect_to = fields.Char(string='Url de Destino')

class WebsiteSimpleApi(models.Model):
    _name = "website.simple.api"

    name = fields.Char(string='Nome da Rota')
    url = fields.Char(string='Rota')
    model = fields.Many2one('ir.model', 'Model')
    fields_to_api = fields.Char(string='Campos para api')
    method_to_use = fields.Selection([('1', 'POST'), 
                                	('2', 'GET')], string='Metodo')
    # url_calculated = fields.Char('Rota Final', compute='_compute_url_final')


    # @api.one
    # def _compute_url_final(self):
    #     '''
    #         Irá computar a url final para utilização
    #     '''
    #     print(self)
