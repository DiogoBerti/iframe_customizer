
from odoo import http, _
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager
from odoo.exceptions import UserError
# iframe_custom_template
import json


class CustomIframe(CustomerPortal):

	@http.route(['/frame/<string:url>'], type='http', auth="public", website=True)
	def reroute_to_redirect(self, url=None, access_token=None, **kw):
		'''
		Gera rotas baseado 
		'''
		route = request.env['website.iframe.custom'].search([('url','=',url)])
		if len(route) > 1:
			raise UserError(_('URL Esta inconsistente'))
		else:
			values = {
				'redirect_to': route[0].redirect_to,
			}
			return request.render("iframe_customizer.iframe_custom_template", values)

class CustomSimpleAPIRoutes(CustomerPortal):

	@http.route(['/api/custom/<string:url>'], type='http', auth="public", website=True)
	def return_api_get(self, url=None, access_token=None, **kw):
		'''
		Busca a rota correta e retorna um Json com os fields 
		'''
		api_route = request.env['website.simple.api'].sudo().search([('url','=',url)])
		if len(api_route) < 1:
			raise UserError(_('URL não existe'))
		elif len(api_route) > 1:
			raise UserError(_('URL Esta inconsistente'))
		model = api_route.model.model
		fields_to_return = api_route.fields_to_api.split(',')
		return_object = request.env[model].sudo().search([])
		string_to_return = []
		
		for item in return_object:
			vals = {}
			for field in fields_to_return:
				try:
					vals.update({field: item[field]})
				except:
					raise UserError(_('Campo %s Não existe na tabela' % field))

			string_to_return.append(vals)
		return json.dumps(string_to_return)

